package kshrd.com.kh;

public class Main {

    public static void main(String[] args) {
        //TODO: INTEGER TYPE;
        ListType<Integer> numberList = new ListType<>();
        try {
            numberList.addItem(3);
            numberList.addItem(1);
            numberList.addItem(2);
            numberList.addItem(1); //THROW EXCEPTION;
          //  numberList.addItem(null); //THROW EXCEPTION;
        }catch (CustomException e){
            System.out.println(e);
        }

        //TODO: ADD 10000 ITEMS EXCEPT[3,1,2] ALREADY EXISTED;
        for (int i=0; i<10000; i++){
            try {
                numberList.addItem(i);
            } catch (CustomException e) {
                System.out.println(e);
            }
        }

        //TODO: MODIFY ITEM BY INDEX;
        numberList.updateItemByIndex(-1,100); //THROW EXCEPTION;
        //TODO: DELETE ITEM BY INDEX;
        numberList.removeItemByIndex(3); //THROW EXCEPTION;
        //TODO: DISPLAY ITEM BY INDEX;
        System.out.println("Index["+2+"]: "+numberList.getItemByIndex(2));
        //TODO: DELETE ALL ITEMS;
        //numberList.removeAllItems();

        //TODO: DISPLAY ALL ITEMS IN NUMBERLIST;
        numberList.getAllItems().forEach(item -> {
            System.out.println(item);
        });

        //TODO: STRING TYPE;
        ListType<String> fruitList = new ListType<>();
        try {
            fruitList.addItem("Banana");
            fruitList.addItem("Apple");
            fruitList.addItem("Durian");
           // fruitList.addItem("Apple"); //THROW EXCEPTION;
            fruitList.addItem(null); //THROW EXCEPTION;
        }catch (CustomException e){
            System.out.println(e);
        }

        //TODO: DISPLAY ALL ITEMS IN FRUITLIST;
        fruitList.getAllItems().forEach(item -> {
            System.out.println(item);
        });


    }
}
