package kshrd.com.kh;

import java.util.ArrayList;

public class ListType<T> {
    private ArrayList<T> myList = new ArrayList<>();

    //TODO: INSERT ITEM INTO MYLIST;
    public void addItem(T item) throws CustomException {
        if (myList.contains(item)){
            throw new CustomException("Element is already EXISTED!");
        }
        if (item == null){
            throw new CustomException("Element can not be NULL!");
        }
        myList.add(item);
    }

    //TODO: MODIFY ITEM BY INDEX IN MYLIST;
    public T updateItemByIndex(Integer index, T newItem){
        try {
            if (!(index >= 0 && index < myList.size())){
                throw new IndexOutOfBoundsException("UPDATE FAILED!");
            }
            if (index == null){
                throw new NullPointerException("UPDATE FAILED!");
            }
            return myList.set(index, newItem);
        }catch (IndexOutOfBoundsException e){
            System.out.println(e);
            return null;
        }catch (NullPointerException e){
            System.out.println(e);
            return null;
        }
    }

    //TODO: DELETE ITEM BY INDEX FROM MYLIST;
    public void removeItemByIndex(Integer index){
        try {
            if (!(index >= 0 && index < myList.size())){
                throw new IndexOutOfBoundsException("CAN NOT REMOVE!");
            }
            if (index == null){
                throw new NullPointerException("CAN NOT REMOVE!");
            }
            myList.remove(index);
        }catch (IndexOutOfBoundsException e){
            System.out.println(e);
        }catch (NullPointerException e){
            System.out.println(e);
        }
    }

    //TODO: DELETE ALL ITEMS FROM MYLIST;
    public void removeAllItems(){
        myList.clear();
    }

    //TODO: RECEIVE ITEM BY INDEX FROM MYLIST;
    public T getItemByIndex(Integer index){
        try {
            if (!(index >= 0 && index < myList.size())){
                throw new IndexOutOfBoundsException("ITEM IS NOT FOUND!");
            }
            if (index == null){
                throw new NullPointerException("ITEM IS NOT FOUND!");
            }
            return myList.get(index);
        }catch (IndexOutOfBoundsException e){
            System.out.println(e);
            return null;
        }catch (NullPointerException e){
            System.out.println(e);
            return null;
        }
    }

    //TODO: RECEIVE ALL ITEMS FORM MYLIST;
    public ArrayList<T> getAllItems(){
        return myList;
    }

}
